# frozen_string_literal: true

require 'date'
require 'httparty'

require 'pry-nav'

# Wrapper for interacting with the GitLab API
class GitLabApiClient # rubocop:todo Metrics/ClassLength
  include HTTParty

  CI_API_GRAPHQL_URL = ENV.fetch('CI_API_GRAPHQL_URL')
  GITLAB_API_TOKEN = ENV.fetch('GITLAB_API_TOKEN')
  FROM_DATE = ENV.fetch('FROM_DATE', (DateTime.now - 3).strftime[..9])
  TO_DATE = ENV.fetch('TO_DATE', (DateTime.now - 1).strftime[..9])

  def author_fragment(include_achievements)
    user_achievements_fragment = <<~EOQUERY
      userAchievements {
        nodes {
          achievement {
            id
          }
        }
      }
    EOQUERY

    <<~EOQUERY
      author {
        id
        username
        name
        avatarUrl
        publicEmail
        bio
        linkedin
        twitter
        discord
        organization
        jobTitle
        location
        createdAt
        #{user_achievements_fragment if include_achievements}
      }
    EOQUERY
  end

  def get_merge_requests(date_field)
    merge_requests = []
    after = nil
    loop do
      puts "Grabbing a page of merge requests based on #{date_field}..."
      result = get_merge_request_page('gitlab-org', date_field, after)
      after = result[:end_cursor]
      merge_requests.concat(result[:merge_requests])

      break unless result[:has_next_page]
    end

    merge_requests
  end

  def get_merge_request_page(group_full_path, date_field, after) # rubocop:todo Metrics/MethodLength
    query = <<~EOQUERY
      query {
        group(
          fullPath: "#{group_full_path}"
        ) {
          mergeRequests(
            includeSubgroups: true
            # first: 25
            after: "#{after}"
            #{date_field}After: "#{FROM_DATE}"
            #{date_field}Before: "#{TO_DATE}"
            labels: ["Community contribution"]
          ) {
            nodes {
              #{author_fragment(false)}
              id
              createdAt
              mergedAt
              title
              description
              webUrl
              reference(full: true)
              labels {
                nodes {
                  title
                }
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    EOQUERY

    response = execute_graphql(query)
    page_info = response.dig('data', 'group', 'mergeRequests', 'pageInfo')
    records = response.dig('data', 'group', 'mergeRequests', 'nodes')
    if records.any?
      puts "Got #{records.count} from #{records.first["#{date_field}At"]} to #{records.last["#{date_field}At"]}"
    else
      puts 'No records found'
    end

    {
      merge_requests: records,
      has_next_page: page_info['hasNextPage'],
      end_cursor: page_info['endCursor']
    }
  end

  def issues
    issues = []
    after = nil
    loop do
      puts 'Grabbing a page of issues...'
      result = get_issues_page('gitlab-org', after)
      after = result[:end_cursor]
      issues.concat(result[:issues])

      break unless result[:has_next_page]
    end

    issues
  end

  def get_issues_page(group_full_path, after) # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
    query = <<~EOQUERY
      query {
        group(
          fullPath: "#{group_full_path}"
        ) {
          issues(
            includeSubgroups: true
            # first: 25
            after: "#{after}"
            createdAfter: "#{FROM_DATE}"
            createdBefore: "#{TO_DATE}"
            confidential: false
          ) {
            nodes {
              #{author_fragment(true)}
              id
              createdAt
              title
              description
              webUrl
              reference(full: true)
              labels {
                nodes {
                  title
                }
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    EOQUERY

    response = execute_graphql(query)
    page_info = response.dig('data', 'group', 'issues', 'pageInfo')
    records = response.dig('data', 'group', 'issues', 'nodes')
    puts "Got #{records.count} from #{records.first['createdAt']} to #{records.last['createdAt']}"

    records = records.reject { |record| team_member?(record['author']) }
    records = records.reject { |record| gitlab_service_account?(record['author']) }
    puts "#{records.count} remaining after filtering out team members..."

    {
      issues: records,
      has_next_page: page_info['hasNextPage'],
      end_cursor: page_info['endCursor']
    }
  end

  def team_member?(author)
    author.dig('userAchievements', 'nodes').any? do |user_achievement|
      user_achievement.dig('achievement', 'id') == 'gid://gitlab/Achievements::Achievement/58'
    end
  end

  def gitlab_service_account?(author)
    author['username'].match?(/^(project|group)_\d+_bot(\w+)?$/) ||
      %w[
        gitlab-bot
        ghost1
        gitlab-qa
        jobbot
        gitlab-dependency-update-bot
      ].include?(author['username'])
  end

  def comments
    # This is going to be a bit more challenging
    # I don't think there is a notes/comments endpoint
    # Cycling through every issue/epic/merge request is a no go
    # We would then need a way to filter the comments to only community members
    # We have a process elsewhere using the team.yml to aid this
  end

  def execute_graphql(query, try = 1) # rubocop:todo Metrics/MethodLength
    cookie_hash = HTTParty::CookieHash.new
    cookie_hash.add_cookies('gitlab_canary=true')
    response = HTTParty.post(
      CI_API_GRAPHQL_URL,
      body: { query: }.to_json,
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer #{GITLAB_API_TOKEN}",
        'Cookie' => cookie_hash.to_cookie_string
      }
    )
    json = JSON.parse(response.body)
    if json['errors']&.any?
      puts json['errors']
      execute_graphql(query, try + 1) if try < 3
    else
      json
    end
  end
end

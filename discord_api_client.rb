# frozen_string_literal: true

require 'httparty'

require 'pry-nav'

# Wrapper for interacting with the Discord API
class DiscordApiClient
  DISCORD_API_BASE_URL = 'https://discord.com/api/v10'
  DISCORD_BOT_TOKEN = ENV.fetch('DISCORD_BOT_TOKEN')

  def get_username_from_id(id)
    return nil if id.empty?

    response = HTTParty.get(
      "#{DISCORD_API_BASE_URL}/users/#{id}",
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bot #{DISCORD_BOT_TOKEN}" }
    )
    user = JSON.parse(response.body)

    { type: 'username', username: user['username'], discriminator: user['discriminator'] }
  end
end

# frozen_string_literal: true

require 'httparty'

require 'pry-nav'
require 'io/console'

# Wrapper for interacting with the Common Room API
class CommonRoomApiClient
  include HTTParty

  base_uri 'https://api.commonroom.io/community/v1'

  API_KEY = ENV.fetch('COMMON_ROOM_API_TOKEN')
  DEFAULT_HEADERS = {
    'Content-Type' => 'application/json',
    'Authorization' => "Bearer #{API_KEY}"
  }.freeze
  DRY_RUN = ENV.fetch('DRY_RUN')

  def actvitiy_types
    self.class.get('/activityTypes', headers: DEFAULT_HEADERS)
  end

  def token_status
    self.class.get('/api-token-status', headers: DEFAULT_HEADERS)
  end

  def create_or_update_member( # rubocop:todo Metrics/CyclomaticComplexity, Metrics/AbcSize, Metrics/MethodLength, Metrics/ParameterLists, Metrics/PerceivedComplexity
    id, username, email, name, avatar_url, bio, linkedin, twitter, discord, organization, job_title, location, joined
  )
    body = {
      user: {
        id: id.to_s,
        fullName: name,
        avatarlUrl: avatar_url,
        bio: bio.empty? ? nil : bio,
        externalProfiles: [{ url: "https://gitlab.com/#{username}" }],
        email: email.nil? || email.empty? ? nil : email,
        linkedin: linkedin.empty? ? nil : { type: 'handle', value: linkedin },
        twitter: twitter.empty? ? nil : { type: 'handle', value: twitter },
        discord:,
        companyName: organization.empty? ? nil : organization,
        titleAtCompany: job_title.empty? ? nil : job_title,
        rawLocation: location.empty? ? nil : location
      },
      id: id.to_s,
      activityType: 'joined',
      timestamp: joined
    }.to_json

    response = self.class.post('/source/22150/activity', headers: DEFAULT_HEADERS, body:) if DRY_RUN == '0'

    if response&.dig('status') == 'ok'
      print "#{username}, "
    else
      puts body
      puts response
    end
  end

  def add_activity( # rubocop:todo Metrics/CyclomaticComplexity, Metrics/AbcSize, Metrics/MethodLength, Metrics/ParameterLists, Metrics/PerceivedComplexity
    activity_type, user_id, id, timestamp, title, description, url, project_full_path, group, type
  )
    tags = []
    tags << { type: 'name', name: group } if group
    tags << { type: 'name', name: type } if type
    { id: id.to_s, activityType: 'made_merge_request' } if activity_type == 'merged_merge_request'
    if description.nil?
      description = ''
    elsif description.length > 90_000
      description = description[..89_999]
    end

    body = {
      id: id.to_s,
      activityType: activity_type, # 'merged_merge_request',  'made_merge_request', 'created_ticket'
      user: { id: user_id.to_s },
      activityTitle: { type: 'text', value: title },
      content: { type: 'markdown', value: description },
      timestamp:,
      url:,
      tags:,
      subSource: { type: 'byName', name: project_full_path }
    }.to_json

    response = self.class.post('/source/22150/activity', headers: DEFAULT_HEADERS, body:) if DRY_RUN == '0'

    if response&.dig('status') == 'ok'
      print "#{url}, "
    else
      puts "\n+++Failed: #{url}"
    end
  rescue => e # rubocop:todo Style/RescueStandardError
    puts "\n+++Failed: #{url}"
    puts body
    puts e
  end
end

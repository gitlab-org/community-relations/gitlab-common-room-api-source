# frozen_string_literal: true

require_relative 'common_room_api_client'
require_relative 'discord_api_client'
require_relative 'gitlab_api_client'

require 'pry-nav'

start = Time.now
authors = []
mrs = []

gitlab_api_client = GitLabApiClient.new

%w[created merged].each do |date_field|
  fetched_mrs = gitlab_api_client.get_merge_requests(date_field)
  mrs.concat(fetched_mrs.map { |mr| { **mr, merged: date_field == 'merged' } }.to_a)
  authors.concat(fetched_mrs.map { |mr| mr['author'] }.to_a)
end

issues = gitlab_api_client.issues
authors.concat(issues.map { |issue| issue['author'] }.to_a)

puts "authors: #{authors.map { |author| author['id'] }.uniq.count} (#{authors.count})"
puts "issues: #{issues.count}"
puts "mrs: #{mrs.count}"
puts '==='

common_room_api_client = CommonRoomApiClient.new

puts 'authors: '
authors.uniq { |author| author['id'] }.each do |author|
  discord = DiscordApiClient.new.get_username_from_id(author['discord'])
  avatar_url = author['avatarUrl']
  avatar_url = "https://gitlab.com#{avatar_url}" if avatar_url[0] == '/'

  common_room_api_client.create_or_update_member(
    author['id'].slice(18..-1),
    author['username'],
    author['publicEmail'],
    author['name'],
    avatar_url,
    author['bio'],
    author['linkedin'],
    author['twitter'],
    discord,
    author['organization'],
    author['jobTitle'],
    author['location'],
    author['createdAt'] || '1970-01-01'
  )
end

puts "\nissues:"
issues.each do |issue|
  common_room_api_client.add_activity(
    'created_ticket',
    issue.dig('author', 'id').slice(18..-1),
    issue['id'].slice(19..-1),
    issue['createdAt'],
    issue['title'],
    issue['description'],
    issue['webUrl'],
    issue['reference'].split('#').first,
    issue.dig('labels', 'nodes').select { |label| label['title'].start_with?('group::') }.first&.dig('title'),
    issue.dig('labels', 'nodes').select { |label| label['title'].start_with?('type::') }.first&.dig('title')
  )
end

puts "\nmerge requests:"
mrs.each do |mr|
  common_room_api_client.add_activity(
    mr[:merged] ? 'merged_merge_request' : 'made_merge_request',
    mr.dig('author', 'id').slice(18..-1),
    mr['id'],
    mr[:merged] ? mr['mergedAt'] : mr['createdAt'],
    mr['title'],
    mr['description'],
    mr['webUrl'],
    mr['reference'].split('!').first,
    mr.dig('labels', 'nodes').select { |label| label['title'].start_with?('group::') }.first&.dig('title'),
    mr.dig('labels', 'nodes').select { |label| label['title'].start_with?('type::') }.first&.dig('title')
  )
end

puts '==='
puts "\nDone in #{Time.now - start} seconds."
